//
//  ListaViewController.swift
//  TesteBancoGames
//
//  Created by Daniel Teodoro on 17/10/2017.
//  Copyright © 2017 Daniel Teodoro. All rights reserved.
//

import UIKit
import Alamofire
import CoreData

class ListaViewController: UIViewController {

    // MARK: - Propriedades
    var gamesArray: NSMutableArray = []
    @IBOutlet weak var collectionView: UICollectionView!
    let sectionInsets = UIEdgeInsets(top: 50.0, left: 20.0, bottom: 50.0, right: 20.0)
    var itemsPerRow: CGFloat!
    var refresher:UIRefreshControl!
    
    // MARK: - Métodos
    override func viewDidLoad() {
        super.viewDidLoad()
        //setup do pull to refresh
        self.refresher = UIRefreshControl()
        self.collectionView!.alwaysBounceVertical = true
        self.refresher.tintColor = UIColor.purple
        self.refresher.addTarget(self, action: #selector(reloadJogosList), for: .valueChanged)
        self.collectionView!.addSubview(refresher)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.calculateItemsPerRow()
        self.reloadJogosList()
    }
    
    //Calcula numero de itens por coluna na baseado na largura da tela
    func calculateItemsPerRow(){
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            itemsPerRow = self.view.frame.width / 125
        } else {
            print("Portrait")
            itemsPerRow = self.view.frame.width / 300
        }
        if(itemsPerRow < 2){
            itemsPerRow = 2
        }
    }
    
    //Verifica conexão antes da requisição
    @objc func reloadJogosList(){
        self.gamesArray = NSMutableArray()
        if NetworkReachabilityManager()!.isReachable{
            self.makeRequest(offset: 0)
        }else{
            print("Erro de conexão")
            let alertController = UIAlertController(title: "Erro", message: "Falha na conexão", preferredStyle: UIAlertControllerStyle.alert)
            let cancelAction = UIAlertAction(title: "Tentar novamente", style: UIAlertActionStyle.cancel) { (result : UIAlertAction) -> Void in
                self.reloadJogosList()
            }
            let okAction = UIAlertAction(title: "Usar dados locais", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                self.loadFromCoreData()
                print("Carregar do CoreData")
            }
            alertController.addAction(cancelAction)
            alertController.addAction(okAction)
            DispatchQueue.main.async{
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        self.calculateItemsPerRow()
    }
    
    //Request da API fazendo utilizando o Offset para controlar os próximos itens a serem requisitados
    func makeRequest(offset: Int){
        let urlString = String.init(format: "https://api.twitch.tv/kraken/games/top?offset=%i", offset)
        let headers: HTTPHeaders = [
            "Client-ID": "uzn9vekxyth2g5ecpf19s48qjwdo4k",
            "Accept": "application/json"
        ]
        Alamofire.request(urlString, headers: headers).responseJSON { response in
            
            if let json = response.result.value {
                let tempJogosArray = ((json as! NSDictionary).object(forKey: "top") as! NSArray)
                
                if self.gamesArray.count == 0{
                    self.deleteAllRecords()
                }
                
                for dic in tempJogosArray {
                    print(dic)
                    let jogo: Jogo = Jogo.from(dic as! NSDictionary)!
                    self.gamesArray.add(jogo)
                    
                    //Salvando com CoreData
                    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
                    let object = NSManagedObject(entity: NSEntityDescription.entity(forEntityName: "Jogos", in: context)!, insertInto: context)
                    object.setValue(jogo.game.name, forKey: "name")
                    object.setValue(jogo.game.logo.large, forKey: "largeImageURL")
                    object.setValue(jogo.channels, forKey: "channels")
                    object.setValue(jogo.viewers, forKey: "viewers")
                    do{
                        try context.save()
                        print("Salvo")
                    }catch{
                        print("Erro ao salvar")
                    }
                }
                self.collectionView.reloadData()
                self.stopRefresher()
            }
        }
    }
    
    func stopRefresher() {
        self.refresher.endRefreshing()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - CoreData
    //Recupera dados do CoreData e preenche Array de jogos
    func loadFromCoreData(){
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Jogos")
        
        request.returnsObjectsAsFaults = false
        do{
            let results = try context.fetch(request)
            if results.count > 0 {
                for result in results as![NSManagedObject]{
                    let gameDic: NSMutableDictionary = NSMutableDictionary()
                    if let name = result.value(forKey: "name") as? String{
                        gameDic.setObject(name, forKey: "name" as NSCopying)
                    }
                    if let large = result.value(forKey: "largeImageURL") as? String{
                        let logo = NSDictionary.init(object: large, forKey: "large" as NSCopying)
                        gameDic.setObject(logo, forKey: "logo" as NSCopying)
                    }
                    let jogoDic: NSMutableDictionary = NSMutableDictionary()
                    jogoDic.setObject(gameDic, forKey: "game" as NSCopying)
                    if let viewers = result.value(forKey: "viewers") as? Int{
                        jogoDic.setObject(viewers, forKey: "viewers" as NSCopying)
                    }
                    if let channels = result.value(forKey: "channels") as? Int{
                        jogoDic.setObject(channels, forKey: "channels" as NSCopying)
                    }
                    let jogo = Jogo.from(jogoDic)!
                    self.gamesArray.add(jogo)
                }
                print("gamesArray: \(self.gamesArray)")
                self.collectionView.reloadData()
                self.stopRefresher()
            }
        }catch{
            print("Erro")
        }
    }
    
    //limpa o CoreData caso nova informação seja carregada da API
    func deleteAllRecords() {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let context = delegate.persistentContainer.viewContext
        
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Jogos")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        
        do {
            try context.execute(deleteRequest)
            try context.save()
            print("CoreData limpo")
        } catch {
            print ("Erro ao limpar CoreData")
        }
    }
}

// MARK: - CollectionView
extension ListaViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return self.gamesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "jogoCell",
                                                      for: indexPath) as! JogoCollectionViewCell
        
        cell.setRank(rank: indexPath.row)
        cell.setCellFrom(jogo: self.gamesArray.object(at: indexPath.row) as! Jogo)
        
        
        //detectando quando é necessário fazer a próxima requisição para o infiniteScroll
        if indexPath.row > self.gamesArray.count - 2 {
            self.makeRequest(offset: self.gamesArray.count)
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = self.collectionView.cellForItem(at: indexPath) as! JogoCollectionViewCell
        cell.infoView.isHidden = false
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = self.collectionView.cellForItem(at: indexPath) as! JogoCollectionViewCell
        cell.infoView.isHidden = true
    }
}

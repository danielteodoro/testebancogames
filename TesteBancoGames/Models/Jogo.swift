//
//  Jogo.swift
//  TesteBancoGames
//
//  Created by Daniel Teodoro on 17/10/2017.
//  Copyright © 2017 Daniel Teodoro. All rights reserved.
//

import Foundation
import Mapper

struct Logo: Mappable {
    var large: String
    
    init(map: Mapper) throws {
        try large = map.from("large")
    }
}
struct Game: Mappable {
    var name: String
    var logo: Logo
    
    init(map: Mapper) throws {
        name = try map.from("name")
        try logo = map.from("logo")
    }
}

struct Jogo: Mappable {
    
    var game: Game
    var viewers: Int
    var channels: Int
    
    init(map: Mapper) throws {
        game = try map.from("game")
        viewers = try map.from("viewers")
        channels = try map.from("channels")
    }
}

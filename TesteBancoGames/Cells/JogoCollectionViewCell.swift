//
//  JogoCollectionViewCell.swift
//  TesteBancoGames
//
//  Created by Daniel Teodoro on 18/10/2017.
//  Copyright © 2017 Daniel Teodoro. All rights reserved.
//

import UIKit
import STXImageCache

class JogoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var gameImage: UIImageView!
    @IBOutlet weak var rankLabel: UILabel!
    @IBOutlet weak var viewersLabel: UILabel!
    @IBOutlet weak var channelsLabel: UILabel!
    @IBOutlet weak var infoView: UIView!
    var rank: Int = 0
    var cellIsSelected: Bool = false
    
    func setCellFrom(jogo: Jogo){
        self.nameLabel.text = jogo.game.name
        let url = URL(string: jogo.game.logo.large)!
        self.gameImage.stx.image(atURL: url)
        self.rankLabel.text = "#\(self.rank+1)"
        self.viewersLabel.text = "Viewers: \(jogo.viewers)"
        self.channelsLabel.text = "Channels: \(jogo.channels)"
    }
    
    func setRank(rank: Int){
        self.rank = rank
    }
    
    override var isSelected: Bool {
        get {
            return super.isSelected;
        }
        
        set {
            if (super.isSelected != newValue) {
                super.isSelected = newValue
                
                if (newValue == true) {
                    self.infoView.isHidden = false
                } else {
                    self.infoView.isHidden = true
                }
            }
        }
    }
}

//
//  JogoCellTest.swift
//  TesteBancoGamesTests
//
//  Created by Daniel Teodoro on 19/10/2017.
//  Copyright © 2017 Daniel Teodoro. All rights reserved.
//

import XCTest
@testable import TesteBancoGames

class JogoCellTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testRankLabel() {
        let cell: JogoCollectionViewCell = JogoCollectionViewCell.init()
        cell.rankLabel = UILabel.init()
        cell.setRank(rank: 1)
        XCTAssertEqual(cell.rank, 1)
    }
}
